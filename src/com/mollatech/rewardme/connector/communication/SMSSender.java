package com.mollatech.rewardme.connector.communication;

public interface SMSSender {
    public RMStatus Load(MobileConnectorSettings setting);
    public RMStatus Unload();
    public String SendMessage(String number, String message);
    public RMStatus GetStatus(String msgid);
    public RMStatus GetServiceStatus();
}
