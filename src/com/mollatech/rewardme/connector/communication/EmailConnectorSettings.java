package com.mollatech.rewardme.connector.communication;

public class EmailConnectorSettings {

    public String smtpIp;
    public int port;
    public String userId;
    public String password;
    public boolean ssl;
    public boolean authRequired;
    public String fromEmail;
    public String fromName;
    public String Subject;
    public Object reserve1;
    public Object reserve2;
    public Object reserve3;
}
