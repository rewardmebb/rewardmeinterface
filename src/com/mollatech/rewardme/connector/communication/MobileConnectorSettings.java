package com.mollatech.rewardme.connector.communication;

public class MobileConnectorSettings {

    public int type;
    public final int SMS = 1;
    public final int USSD = 2;
    public final int VOICE = 3;
    public String ip;
    public int port;
    public String userid;
    public String password;
    public String className;
    public boolean logConfirmation;
    public String phoneNumber;
    public Object reserve1;
    public Object reserve2;
    public Object reserve3;
}
