package com.mollatech.rewardme.connector.communication;

public interface EmailSender {

    public RMStatus Load(EmailConnectorSettings setting);

    public RMStatus Unload();

    public String SendMessage(String emailid, String subject, String message, String[] cc, String[] bcc, String[] files, String[] mimetypes);

    public RMStatus GetStatus(String msgid);

    public RMStatus GetServiceStatus();
}
